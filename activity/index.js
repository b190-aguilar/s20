// console.log("Hello World!");

let userNumber = parseInt(prompt("Please input a number: "));

function divisionCheck(num){
	console.log("The number you provided is: " + num);

	for(let x = num; x >= 50; x--){
		// if 50, terminate the loop
		if (x === 50) {
			console.log("The current value is at 50. Terminating the loop.");
			break;
		}
		// if divisible by 10, print "number x is being skipped"
		else if(x % 10 === 0){
			console.log("The number is divisible by 10. Skipping the number");
		}

		// if divisible by 5, print number
		else if(x % 5 === 0){
			console.log(x);
		}
	};
};

divisionCheck(userNumber);

function stringCheck(){
	let myString = "supercalifragilisticexpialidocious";
	let newString = "";

	console.log(myString);

	for (let x = 0; x < myString.length; x++) {
		if (myString[x].toLowerCase() === "a" ||
			myString[x].toLowerCase() === "e" ||
			myString[x].toLowerCase() === "i" ||
			myString[x].toLowerCase() === "o" ||
			myString[x].toLowerCase() === "u"){
				continue;
			}
		else{	
		// add element to the string if not a vowel
		newString = newString + myString[x] ;
		}
			
	}

	console.log(newString);
}

stringCheck();