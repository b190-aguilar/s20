

// FOR Loop
	/*
		the most flexible loop compared to do-while and while loops. it has 3 parts
			1. initialization - tracks the progression of the loop
			2. condition/expression - will be evaluated. will determine if the loop will run one more time
			3. finalExpression - indicates how to advance the loop


		Stages of FOR Loop
			- will initialize a variable "count" that has a value of 0;
			- the condition/ expression that is to be assessed is if the value of "count" is less than or equal to 20
			- increment the value of count
	*/

	for (let count = 0; count <= 20; count ++){
	console.log("For Loop: " + count);
	}


// using Strings

/*
	characters in a string may be counted using .length property
		- .length is commonly used in arrays, to check the number of elements in the array.
		- in measures the number of elements, and returns an integer.
*/


let myString = "alex";
console.log(myString);


for (let x=0; x < myString.length; x++) {
	console.log(myString[x]);	
}

/*
	the fxn below looks like we're performing the following; these are also examples on how we can get access to the characters inside the string itself.
		console.log(myString[0]);
		console.log(myString[1]);
		console.log(myString[2]);
		console.log(myString[3]);
*/

/*
	strings as special compared to other data types that it has access to fxns and other pieces of information another data type might not have 
*/

// MINIACTIVITY
/*
	create a let variable myName that has your name as the value
		using for loop, log in the console each character of the variable but make sure that the string will not be case-sensitive

		if character is vowel, display 3
*/
console.log("-----")


let myName = "James"

for (let x = 0; x < myName.length; x++) {
	switch(myName[x].toLowerCase()){
		case 'a' :
			console.log(3);
			break;

		case 'e' :
			console.log(3);
			break;
				
		case 'i' :
			console.log(3);
			break;
				
		case 'o' :
			console.log(3);
			break;
				
		case 'u' :
			console.log(3);
			break;
				

		default :
			console.log(myName[x].toLowerCase());
	}
}


// CONTINUE and BREAK statements

for(let count = 0; count <= 20; count++){
	if (count % 2 === 0){
		continue;
	};
	console.log("Continue and Break: " + count);
	if (count > 10){
		break;
	};
};

// CONTINUE statement - allows the code to move to the next iteration of the loop without finishing the statements in the code block.
// BREAK statement - terminates/ends the loop 


// MINIACTIVIY
/*
	create a FOR loop with CONTINUE and BREAK statements
		inside the FOR loop, try to log in the console each letter of the name variable and with a catch.
			if letter is a, continue to next iteration
			if letter is d, break the loop 
*/

let newName = "james benedict";


for (let i = 0; i < newName.length; i++ ){
	// console.log(newName[i]); - will display 
	if (newName[i].toLowerCase() === "a"){
		continue;
	}
	// console.log(newName[i]); - will not display the a's in the string
	if (newName[i].toLowerCase() === "d"){
		break;
	}
	console.log(newName[i]); 
		// - will not display all of the a's and d's in the string.
}


function loopInsideLoop() {
	for(let x = 0; x <= 3; x++){
		for(let y = 0; y <= x; y++){
			console.log("x: " + x + " y: " + y);
		}
	}

}

loopInsideLoop();